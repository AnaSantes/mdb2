    package mis.pruebas.productosmongodb.servicio.impl;

import com.mongodb.client.result.UpdateResult;
import mis.pruebas.productosmongodb.modelo.Producto;
import mis.pruebas.productosmongodb.servicio.RepositorioProductoPersonalizado;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

public class RepositorioProductoPersonalizadoImpl implements RepositorioProductoPersonalizado {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public List<Producto> buscarPorRangoPreciosPersonalizado(double min, double max) {

        System.out.println(String.format("===== buscarPorRangoPreciosPersonalizado(%f, %f)",min,max));

        Query q = new Query();
        q.addCriteria(Criteria.where("precio").lte(max));

        return this.mongoOperations.find(q, Producto.class);
    }

    @Override
    public void ajustarPrecioProducto(String id, double precio) {
        System.out.println(String.format("===== ajustarPrecioProducto('%s', %f)",id, precio));

        final Query q = new Query();
        q.addCriteria(Criteria.where("_id").is(new ObjectId(id)));

        final Update u = new Update();
        u.set("precio", precio);

        final UpdateResult r = this.mongoOperations.updateFirst(q, u, Producto.class);
        System.out.println(String.format("===== ajustarPrecioProducto('%s', %f): acknowledged: %b, matched: %d, modified: %d",
                id,
                precio,
                r.wasAcknowledged(),
                r.getMatchedCount(),
                r.getModifiedCount()));

    }
}
