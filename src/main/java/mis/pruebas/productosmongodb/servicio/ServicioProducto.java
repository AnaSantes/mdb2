package mis.pruebas.productosmongodb.servicio;

import mis.pruebas.productosmongodb.modelo.Producto;

import java.util.List;

public interface ServicioProducto {
    public void agregar(Producto p);
    public Producto buscarPorId(String id);
    public List<Producto> obtenerTodos();
    public void guardarProductoPorId(String idProducto, Producto p);
    public List<Producto> buscarPorRangoPrecio(double minPrecio, double maxPrecio);
    public List<Producto> buscarPorProveedor(String nombreProveedor);

    public void ajustarPrecioProductoPorId(String idProducto, double precio);
}
