package mis.pruebas.productosmongodb.controlador;

import mis.pruebas.productosmongodb.seguridad.Credenciales;
import mis.pruebas.productosmongodb.seguridad.JwtBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/autenticacion/token")
public class ControladorAutenticacion {

    @Autowired
    JwtBuilder jwtBuilder;

    @PostMapping
    public String autenticar(@RequestBody Credenciales credenciales) {
        // 1. validar las credenciales
        // 1. generar token con lista de "claims" o "declaraciones".

        // FIXME: validar contra base de datos.
        if(!credenciales.usuario.equalsIgnoreCase("andres")
            || !credenciales.contrasenia.equals("1234")) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        return jwtBuilder.generarToken(credenciales.usuario);
    }
}
